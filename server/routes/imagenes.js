const express = require('express');
const fs = require('fs');
const path = require('path');

const { verificaTokenImg } = require('../middlewares/autenticacion')

let app = express();


app.get('/imagen/:tipo/:img', verificaTokenImg, (req, res) => {
    let tipo = req.params.tipo;
    let nombreImagen = req.params.img;


    const pathUploads = path.resolve(__dirname, `../../uploads/${tipo}/${nombreImagen}`);
    let pathImg = fs.existsSync(pathUploads)
        ? pathUploads : path.resolve(__dirname, `../assets/no-image.jpg`);

    // devuelve el tipo que sea, lee el content-type del archivo y lo devuelve
    res.sendFile(pathImg);
});



module.exports = app;