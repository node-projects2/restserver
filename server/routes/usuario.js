const express = require('express');
const app = express();
const _ = require('underscore');

const bcrypt = require('bcrypt');

const Usuario = require('../models/usuario');

const { verificaToken, verificaAdminRole } = require('../middlewares/autenticacion');

app.get('/usuario', verificaToken, (req, res) => {

    // return res.json({
    //     usuario: req.usuario,
    //     nombre: req.usuario.nombre,
    //     email: req.usuario.email
    // });

    let desde = req.query.desde || 0;
    desde = Number(desde);
    let limite = req.query.limite || 5;
    limite = Number(limite);


    // con el string podemos indicar los elementos que queremos mandar
    Usuario.find({ estado: true }, 'nombre email role estado img')
        .skip(desde)
        .limit(limite)
        .exec((err, usuariosDB) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    mensaje: err
                });
            }

            Usuario.countDocuments({ estado: true }, (err, conteo) => {

                res.json({
                    ok: true,
                    usuarios: usuariosDB,
                    total: conteo
                });
            });

        });
    // res.json('get usuarios LOCAL!!')
});
app.post('/usuario', [verificaToken, verificaAdminRole], function (req, res) {
    let body = req.body;
    const numeroVueltasDelBCrypt = 10;

    let usuario = new Usuario({
        nombre: body.nombre,
        email: body.email,
        password: bcrypt.hashSync(body.password, numeroVueltasDelBCrypt),
        role: body.role
    });

    usuario.save((err, usuarioDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                mensaje: err
            });
        }

        // usuarioDB.password = null;
        res.json({
            ok: true,
            usuario: usuarioDB
        });
    });
});
app.put('/usuario/:id', [verificaToken, verificaAdminRole], function (req, res) {
    let urlId = req.params.id;
    // creamos copia del objeto con sólo las opciones que son modificables con underscore
    let body = _.pick(req.body, ['nombre', 'email', 'img', 'role', 'estado']);
    // let body = req.body;

    // para quitar los campos que no queremos que se cambien podríamos hacer esto:
    // delete body.password;
    // delete body.google;


    // la opción de new: true es para que se devuelva el objeto modificado y no el objeto antes de modificarse
    // runvalidators para que pase las validaciones a la hora de guardar
    Usuario.findByIdAndUpdate(urlId, body, { new: true, runValidators: true }, (err, usuarioDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                mensaje: err
            });
        }


        res.json({
            ok: true,
            usuario: usuarioDB
        });

    });
});
app.delete('/usuario/:id', [verificaToken, verificaAdminRole], function (req, res) {
    let id = req.params.id;

    // Usuario.findByIdAndRemove(id, (err, usuarioBorrado) => {
    let cambiaEstado = {
        estado: false
    };
    Usuario.findByIdAndUpdate(id, cambiaEstado, { new: true }, (err, usuarioBorrado) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                mensaje: err
            });
        }

        if (!usuarioBorrado) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: 'Usuario no encontrado'
                }
            });
        }

        res.json({
            ok: true,
            usuario: usuarioBorrado
        });

    })
    // res.json('delete usuarios')
});

module.exports = app;